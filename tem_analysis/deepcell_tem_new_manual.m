homeFolder = '/Volumes/External HDD2 1/cell wall thickness data/Technai/';
techNames = dir(homeFolder);
temp = {techNames.name};
temp2 = ~contains(temp,'.');
techFolderNames = temp(temp2);
homeFolder = '/Volumes/External HDD2 1/cell wall thickness data/Hitachi/';
techNames = dir(homeFolder);
temp = {techNames.name};
temp2 = ~contains(temp,'.');
hitFolderNames = temp(temp2);
allFolderNames = cat(2, techFolderNames, hitFolderNames);

resizeFolder = '/Volumes/External HDD2 1/cell wall thickness data/tem_new/';
resizeNames = dir(resizeFolder);
resizeNames = natsortfiles({resizeNames.name});
resizeNames = resizeNames(~contains(resizeNames,'.'));
%resizeNames = resizeNames([1:8,12:31]);
segmentFolder = '/Volumes/External HDD2 1/cell wall thickness data/tem_new_segment/tem_new/';
segmentNames = dir(segmentFolder);
segmentNames = natsortfiles({segmentNames.name});
segmentNames = segmentNames(~contains(segmentNames,'.'));
%
for folderNo = 1:length(resizeNames)
    resizeImageFolder = dir(strcat(resizeFolder,resizeNames{folderNo}));
    resizeImageFolderNames = natsortfiles({resizeImageFolder.name});
    resizeImageFolderNames = resizeImageFolderNames(contains(resizeImageFolderNames,'.tif'));
    segmentImageFolder = dir(strcat(segmentFolder,strcat('test_',resizeNames{folderNo})));
    segmentImageFolderNames = natsortfiles({segmentImageFolder.name});
    segmentImageFolderNames = segmentImageFolderNames(contains(segmentImageFolderNames,'.png'));
    for imageNo = 1:length(resizeImageFolderNames)
        if ~isempty(thickStruct(folderNo,imageNo).diameter)
            continue
        end
        [folderNo, imageNo]
        TifLink = Tiff(strcat(resizeFolder,resizeNames{folderNo},'/',resizeImageFolderNames{imageNo}), 'r');
        TifLink.setDirectory(1);
        resizeImage=TifLink.read();
        TifLink.close();
        clear TifLink
        g = figure;
        imshow(resizeImage,[mean(double(resizeImage(:)))-(std(double(resizeImage(:)).*3)) mean(double(resizeImage(:)))+(std(double(resizeImage(:)).*3))]);
        g = set(g,'Position',[345 1104 560 420]);
        'press enter to measure image, click figure to move on to next image'
        w = NaN;
        w = waitforbuttonpress;
        close all;
        if w == 0
            imageNo = imageNo + 1;
            if imageNo > length(resizeImageFolderNames)
                folderNo = folderNo + 1;
                if folderNo > length(resizeNames)
                    'done'
                    pause();
                end
                break
            end
            continue
        end
        cytoImage = imread(strcat(segmentFolder,strcat('test_',resizeNames{folderNo}),'/','feature_0_frame_',num2str(imageNo-1),'.png'));
        outImage = imread(strcat(segmentFolder,strcat('test_',resizeNames{folderNo}),'/','feature_1_frame_',num2str(imageNo-1),'.png'));
        C = {};
        for i = 1:200   
            %imshow(resizeImage,[]); 
            f = figure
            set(f,'units','normalized','outerposition',[ -0.24609, 1, 1.5,1.3213])
            lineIm = subplot(1,2,1);
            imshow(resizeImage,[mean(double(resizeImage(:)))-(std(double(resizeImage(:)).*4)) mean(double(resizeImage(:)))+(std(double(resizeImage(:)).*4))]);
            hold on
            rectangle('Position',[30 30 size(resizeImage,2)-60 size(resizeImage,1)-60]);
            cytoIm = subplot(1,2,2);
            imshow(cytoImage+outImage,[])
            hold on 
            ax = gca;
            ax.Position = [0.5 0 0.5 1];
            lineIm.Position = [0 0 0.5 1];
            if ~isempty(C)
                hold on
                plot(lineIm,C{i-1}(:,1), C{i-1}(:,2)) 
                hold on
                plot(cytoIm,C{i-1}(:,1), C{i-1}(:,2)) 
            end
            [x, y] = getline(lineIm);
            C{i} = [x, y]; 
            clear x y
            'press enter to add line, click figure to end line input'
            w=NaN;
            w = waitforbuttonpress;
            close all
            if w == 0
                break
            end
        end
        temp_var_k=0;
        while temp_var_k<10
            'diameter'
            g = figure;
            imshow(resizeImage,[mean(double(resizeImage(:)))-(std(double(resizeImage(:)).*3)) mean(double(resizeImage(:)))+(std(double(resizeImage(:)).*3))]);
            g = set(g,'Position',[345 1104 560 420]);
            [x, y] = getline;
            diameter = hypot(diff(x), diff(y));
            'press enter to continue, click figure to remeasure'
            w=NaN;
            w = waitforbuttonpress;
            if w == 1
                close all;
                break
            end
            temp_var_k = temp_var_k+1;
        end
        len = 30;
        E = {};
        D = {};
        F = {};
        G = {};
        clear profile profile1 profile2    
        for i = 1:size(C,2) 
            a = C{i}(:,1);  
            b = C{i}(:,2);
            diffx = diff(a);
            diffy = diff(b);
            t = zeros(1,length(a)-1);
            for n = 1:length(a)-1 
                t(n+1) = t(n) + sqrt(diffx(n).^2+diffy(n).^2);
            end
            tj = linspace(t(1),t(end),round(t(end)));
            xj = interp1(t,a,tj);
            yj = interp1(t,b,tj);
            for j = 1:(size(xj,2)-1)
                thet = atan2((yj(j) - yj(j+1)),(xj(j) - xj(j+1)));
                xx = [xj(j) + (cos(thet + pi/2).*len), xj(j) + (cos(thet - pi/2).*len)];
                yy = [yj(j) + (sin(thet + pi/2).*len), yj(j) + (sin(thet - pi/2).*len)];
                [xd{i, j}, yd{i, j}, profile(:,j)] = improfile(resizeImage,xx,yy,len.*2);
                profile1(:,j) = improfile(cytoImage,xx,yy,len.*2);
                profile2(:,j) = improfile(outImage,xx,yy,len.*2);
                clear xx yy thet 
            end
            D{i} = [xj',yj'];
            E{i} = profile;
            F{i} = profile1;%cyto
            G{i} = profile2;%out
            clear profile profile1 profile2 xj yj t tj a b diffx diffy
        end

        meanE={};
        meanF={};
        meanG={};
        num = 10;
        for k = 1:size(E,2)
            for i = 1:(2*(size(E{k},2)./num))
                num1 = i.*(num/2) - ((num/2)-1);
                num2 = num1+num-1;
                if num2 < size(E{k},2)
                    meanE{k}(:,i) = nanmean(E{k}(:,num1:num2),2);
                    meanF{k}(:,i) = nanmean(F{k}(:,num1:num2),2);%cyto
                    meanG{k}(:,i) = nanmean(G{k}(:,num1:num2),2);%out
                end
                clear lol
            end
        end
        peakLocs = {};
        F_norm_out = {};
        G_norm_out = {};
        E_norm_out = {};
        clear cytoImage outImage
        for j = 1:size(meanE,2)
            for i = 1:size(meanE{j},2)
                %workingCurve = meanE{j}(:,i);
                workingE = meanE{j}(:,i);
                workingF = meanF{j}(:,i);%cyto
                workingG = meanG{j}(:,i);%out
                if (size(findpeaks(workingF,'MinPeakProminence',3000),1)>1) || (size(findpeaks(workingG,'MinPeakProminence',3000),1)>1)
                    locs2 = [];
                else
                    F_x = fit([1:size(workingF,1)]',workingF,'gauss1','Upper',[60000 len*2 Inf]);
                    G_x = fit([1:size(workingG,1)]',workingG,'gauss1','Upper',[60000 len*2 Inf]);
                    locs2 = [F_x.b1 G_x.b1];%[cyto out]
                end
                if ~isempty(locs2)
                    peakLocs{i,j} = locs2;%sort(locs2);
                    if locs2(1)>locs2(2)
                        shiftAmount = (len/2)+(locs2(1) - len*2);
                        workingE = flipud(workingE);
                        workingF = flipud(workingF);
                        workingG = flipud(workingG);
                        E_norm_out{i,j} = circshift(workingE,round(shiftAmount));
                        F_norm_out{i,j} = circshift(workingF,round(shiftAmount));%cyto
                        G_norm_out{i,j} = circshift(workingG,round(shiftAmount));%out
                    else
                    shiftAmount = (len/2)-locs2(1);
                    E_norm_out{i,j} = circshift(workingE,round(shiftAmount));
                    F_norm_out{i,j} = circshift(workingF,round(shiftAmount));
                    G_norm_out{i,j} = circshift(workingG,round(shiftAmount));
                    end
                end
            end
        end
        %
        diffLocs = cellfun(@diff, peakLocs,'UniformOutput',0);
        sizeList = cellfun(@size, diffLocs,'UniformOutput',0);
        sizeList = cat(1,sizeList{:});
        sizeList2 = cellfun(@size, diffLocs,'UniformOutput',0);
        %thicknesses = abs(cat(2,diffLocs{sizeList(:,1)==1}));
        lineDraw={};
        lineCollection = {};
        %imshow(resizeImage,[])
        %hold on
        %radLen = (500+250)/2;
        for j = 1:size(meanE,2)
            for i = 1:size(sizeList2,1)
                if sum(sizeList2{i,j})==2
                    num1 = i.*(num/2) - ((num/2)-1);
                    num2 = num1+num-1;
                    meanLocs = round(mean([num1 num2]));
                    roundPeak = round(peakLocs{i,j});
                    roundPeak(roundPeak<=0)=1;
                    x1 = [xd{j, meanLocs}(roundPeak)];
                    y1 = [yd{j, meanLocs}(roundPeak)];
                    [~,idx] = sort(peakLocs{i,j});
                    x1 = flipud(x1);
                    y1 = flipud(y1);
                    lineDraw{i,j} = [x1, y1];
                    %radThet = atan(diff(x1)/diff(y1));
                    %tempSign = sign(diff(y1));
                    %x2 = x1(1)+(tempSign.*(radLen.*sin(radThet))); 
                    %y2 = y1(1)+(tempSign.*(radLen.*cos(radThet)));
                    %a = [x1; x2];
                    %b = [y1; y2];
                    %diffx = diff(a);
                    %diffy = diff(b);
                    %t = zeros(1,length(a)-1);
                    %for n = 1:length(a)-1 
                    %    t(n+1) = t(n) + sqrt(diffx(n).^2+diffy(n).^2);
                    %end
                    %tj = linspace(t(1),t(end),round(t(end)));
                    %xj = interp1(t,a,tj);
                    %yj = interp1(t,b,tj);
                    %lineCollection{i,j} = [xj;yj]';
                    %plot(x1, y1,'.');
                    %hold on
                end
            end
        end
        thickStruct(folderNo,imageNo).measureLines = C;
        thickStruct(folderNo,imageNo).diameter = diameter;
        thickStruct(folderNo,imageNo).peakLocs = peakLocs;
        thickStruct(folderNo,imageNo).diffLocs = diffLocs;
        thickStruct(folderNo,imageNo).normImInt = E_norm_out;
        thickStruct(folderNo,imageNo).normCytoInt = F_norm_out;
        thickStruct(folderNo,imageNo).normOutInt = G_norm_out;
        thickStruct(folderNo,imageNo).linePlot = lineDraw;
        thickStruct(folderNo,imageNo).imageName = strcat(resizeFolder,resizeNames{folderNo},'/',resizeImageFolderNames{imageNo});
        thickStruct(folderNo,imageNo).maskName = strcat(segmentFolder,strcat('test_',resizeNames{folderNo}),'/','feature_0_frame_',num2str(imageNo-1),'.png');       
        save('thickStruct.mat','thickStruct');
    end
end