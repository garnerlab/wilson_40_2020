function idx_out = iterative_extend(all_vals,all_range,idx_in)
    vals_min = min(all_vals(idx_in)-all_range(idx_in));
    vals_max = max(all_vals(idx_in)+all_range(idx_in));
    vals_in_range_map = all_vals>vals_min & all_vals<vals_max;
    vals_in_range = all_vals(vals_in_range_map);
    if sum(~ismember(all_vals(idx_in),vals_in_range)>0)
        idx_out = iterative_extend(all_vals,all_range,vals_in_range_map);
    else
        idx_out = vals_in_range_map;
    end
end