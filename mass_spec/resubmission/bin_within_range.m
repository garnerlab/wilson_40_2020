function out = bin_within_range(vals,range)
    [sort_vals,vals_idx] = sort(vals);
    [~, reverse_sort] = sort(vals_idx);
    vals = sort_vals;
    range = range(vals_idx);
    vals_min = vals-range;
    vals_max = vals+range;
    counter = 1;
    idx_out = zeros(length(vals));
    for i = 1:length(vals)
        vals_in_range = vals>vals_min(i) & vals<vals_max(i);
        vals_in_range_out = iterative_extend(vals,range,vals_in_range);
        idx_out(:,i) = vals_in_range_out;
    end
    CC = bwconncomp(idx_out,4);
    idxs_out_final = zeros(size(vals));
    for i = 1:CC.NumObjects
        [I,~] = ind2sub(CC.ImageSize,CC.PixelIdxList{i});
        idxs_out_final(unique(I))=i;
    end
    out = idxs_out_final(reverse_sort);
end