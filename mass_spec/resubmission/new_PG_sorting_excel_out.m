[max_quant_file, directory] = uigetfile('.tsv');
%%
direct_contents = dir(directory);
file_names = natsortfiles({direct_contents.name});
file_names = file_names(contains(file_names,{'SAM','blank'}));
file_names_split = cellfun(@(x) strsplit(x,{' ','.'}),file_names,'UniformOutput',0);
file_names_split = cellfun(@(x) x{2}, file_names_split,'UniformOutput',0);
unique_raw_file_strings = unique(file_names_split);
for i = 1%:numel(unique_raw_file_strings)  
    file_names_search = file_names(contains(file_names,unique_raw_file_strings{i}));
    %clear max_quant_data
    temp = tdfread(strcat(directory,file_names_search{1}));
    temp2 = tdfread(strcat(directory,file_names_search{2}));
    f = fieldnames(temp);
    for k = 1:length(f)
        max_quant_data{i}.(f{k}) = {temp.(f{k}), temp2.(f{k})};
    end
end

fileNames = dir(strcat(directory,'..'));
sortTemp = natsortfiles({fileNames.name});
temp2 = contains(sortTemp,'SAM'); %a string appearing in all filenames
finalFileNames = sortTemp(temp2);
temp2 = ~contains(finalFileNames,'.mat'); %a string appearing in all filenames
finalFileNames = finalFileNames(temp2);


%% generate theoretical masses for PG base and modified PG (deacylation, loss of glucosamine, etc)
C13 = 0;
wallStrings = {
                'M'
                'MA'
                'MAG'
                'MAGD'
                'MAGDADTX'
                'MAGDADGTX'
                'MAGDADGATX'
                'MAGDADGAMX'
                'MAGDADGAMADTXX'
                'MAGDADGAMADGTXX'
                'MAGDADGAMADGATXX'
                'MAGDADGAMADGAMXX'
                'MAGDADGAMADGAMADTXXX'
                'MAGDADGAMADGAMADGTXXX'
                'MAGDADGAMADGAMADGATXXX'
                'MAGDADGAMADGAMADGAMXXX'
                };
%
Elements = [19	34	2	13	0	0%M
            -8	-13	-1	-5	0	0%-Glc
            -2	-2	0	-1	0	0%Ac
            0	0	0	3	1	0%P
            0	-4	0	-1	0	0%anh
            3	5	1	1	0	0%Ala
            5	7	1	3	0	0%Glu
            7	12	2	3	0	0%Dap
            0	-2	0	-1	0	0%Xlink
            0	2	0	1	0	0%T
            0	1	1	-1	0	0%NH2
            0	1	0	0	0	0
            13	34	2	13	0	6];%MC13
Symbols = {'C' 'H'	'N'	'O'	'P'	'C13'};
Names = {'M' '-Glc' 'Ac' 'P' 'anh' 'A' 'G' 'D' 'X' 'T' 'NH2' 'H' 'MC13'};
clear PG
for i = 1:size(wallStrings,1)
    letter = wallStrings{i};
    PG{i}.Elements = zeros(1,size(Elements,2));
    for j = 1:numel(letter)
        switch letter(j)
            case 'M'
                if ~C13
                    PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'M'),:);
                else
                    PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'MC13'),:);
                end
            case 'A'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'A'),:);
            case 'G'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'G'),:);
            case 'D'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'D'),:);
            case 'X'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'X'),:);
            case 'T'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'T'),:);
        end
    end
    PG{i}.Name = letter;
end
PGsize = size(PG,2);
%
lol = 4;
for i = lol:PGsize
    PG{i+PGsize-(lol-1)}.Elements = PG{i}.Elements + Elements(strcmp(Names,'A'),:);
    PG{i+PGsize-(lol-1)}.Name = strcat(PG{i}.Name , 'A');
    PG{i+(2.*PGsize)-((lol-1)*2)}.Elements = PG{i}.Elements + 2.*Elements(strcmp(Names,'A'),:);
    PG{i+(2.*PGsize)-((lol-1)*2)}.Name = strcat(PG{i}.Name , 'AA');
end
%
for i = 1:size(PG,2)
        PG{i}.Glc{1} = PG{i}.Elements + Elements(strcmp(Names,'-Glc'),:);
        PG{i}.Phos{1} = PG{i}.Elements + Elements(strcmp(Names,'P'),:);
        PG{i}.anh{1} = PG{i}.Elements + Elements(strcmp(Names,'anh'),:);
    for j = 1:sum(PG{i}.Name=='M')
        PG{i}.Acyl{j} = PG{i}.Elements + j.*Elements(strcmp(Names,'Ac'),:);
    end
end
%
for i = 1:size(PG,2)
    for j = 1:sum(PG{i}.Name=='D')
        PG{i}.NH2{j} = PG{i}.Elements + j.*Elements(strcmp(Names,'NH2'),:);
        if isfield(PG{i},'Acyl')
            for k = 1:size(PG{i}.Acyl,2)
                PG{i}.NH2Acyl{j,k} = PG{i}.Acyl{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
        if isfield(PG{i},'Glc')
            for k = 1:size(PG{i}.Glc,2)
                PG{i}.NH2Glc{j,k} = PG{i}.Glc{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
        if isfield(PG{i},'Phos')
            for k = 1:size(PG{i}.Phos,2)
                PG{i}.NH2Phos{j,k} = PG{i}.Phos{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
        if isfield(PG{i},'anh')
            for k = 1:size(PG{i}.anh,2)
                PG{i}.NH2anh{j,k} = PG{i}.anh{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
    end
end
%% isotopicdist_better is like isotopicdist, but faster
[Compound, PGLabel] = PGstruct(PG,0);
MDs_base = cellfun(@(x) x(1),isotopicdist_better(Compound));
[Compound_Glc, PGLabel_Glc] = PGstruct(PG,1);
MDs_base_Glc = cellfun(@(x) x(1),isotopicdist_better(Compound_Glc));
%              H        Na             2Na        K            2K     
%MDs_adducts = [0, +21.98194464,+43.96388928,+37.95588187,+75.91176374];
MDs_adducts = [0, +21.98194464,+43.96388928,+65.94583391];%,+37.95588187,+75.91176374,+113.8676456];
adducts_str = ['','1Na','2Na','3Na'];
%MDs_adducts = 0;
decay_mass = -203.0793725;
Acyl = MDs_base(ismember(PGLabel,'MAG'))-MDs_base(ismember(PGLabel,'MAG1Acyl'));
%%
names_mat = {'1SB-1','431SB-1','461-1','523-1','526-1','541-1','542-1','1SC-1','431SC-1','1UC-1','431UC-1','1SB-2','431SB-2','461-2','523-2','526-2','541-2','542-2','1SC-2','431SC-2','1UC-2','431UC-2','1SB-3','431SB-3','461-3','523-3','526-3','541-3','542-3','1SC-3','431SC-3','1UC-3','431UC-3','NAM-H2O','NAM-T'};
RT_tol=1;
ppm_tol=10;
for i = 1:numel(unique_raw_file_strings)-2
    fprintf([unique_raw_file_strings{i},' sample ',num2str(i),'\n'])
    for GlcPG=[0,1]
        %the_table = str2double(max_quant_split.(unique_raw_file_strings{i}).data);
        mass_to_analyze = max_quant_data{i}.mass{2};
        mass_to_analyze(max_quant_data{i}.charge{2}>3)=0;
        RT_to_analyze =  max_quant_data{i}.rtApex{2};
        start_times = max_quant_data{i}.rtStart{2};
        end_times = max_quant_data{i}.rtEnd{2};
        
        RL_to_analyze =  (end_times-start_times)./2;
        I_to_analyze = max_quant_data{i}.intensityApex{2};  
        IDX_to_analyze = 1:length(I_to_analyze)';
        range_to_run = [7,80];
        
        mzXML = mzxmlread(strcat(directory,'../',finalFileNames{i}));
        %PeakList = MSStruct{i}.PeakList;
        %Times = MSStruct{i}.Times;
        [PeakList, Times] = mzxml2peaks(mzXML);
        
        MSStruct{i}.mzXML = mzXML;
        MSStruct{i}.PeakList = {PeakList};
        MSStruct{i}.Times = Times;
        %parse .mzXML files for + and - scans from 400-4000
        MSStruct{i}.mzXML = {};
        peakListSize = size(PeakList,1);
        posPeakList = PeakList(3:4:peakListSize-rem(peakListSize,4));
        posTimeList = Times(3:4:peakListSize-rem(peakListSize,4));
        MSStruct{i}.posPeakList = {posPeakList};
        %clear PeakList
        temp = cellfun(@sum,posPeakList,'UniformOutput',false);
        for k = 1:size(temp,1)
            TIC_pos(k) = temp{k}(2);
        end  
        MSStruct{i}.TIC_pos = {TIC_pos};
        
        I_to_analyze_filtered = nan(size(I_to_analyze,1),length(posTimeList));
        
        for j = 1:length(posTimeList)
            time_now = posTimeList(j)/60;
            to_sort = (start_times<time_now)&(end_times>time_now);
            if sum(to_sort)>0
                mz_to_sort=max_quant_data{i}.mz{2}(to_sort);
                peak_list = MSStruct{i}.posPeakList{1}{j};
                mz_time_now = peak_list(:,1);
                I_out_time_now = zeros(size(mz_to_sort));
                for masses = 1:length(mz_to_sort)
                    [minval,idx] = min(abs(mz_time_now-mz_to_sort(masses)));
                    if minval<0.1
                        I_out_time_now(masses) = peak_list(idx,2);
                        temp = peak_list(idx,2);
                    end
                end
                sort_I = sort(unique(I_out_time_now),'descend');
                if ~isempty(nonzeros(sort_I))
                    len_num = min([length(nonzeros(sort_I)) 10]);
                    top_10_I = I_out_time_now>=sort_I(len_num);
                    idx_in = find(to_sort);
                    idx_out = idx_in(top_10_I);             
                    I_to_analyze_filtered(idx_out,j) = I_out_time_now(top_10_I);
                end
            end
        end
        I_to_analyze_filtered(I_to_analyze_filtered==0)=nan;
        mass_to_analyze(isnan(nanmean(I_to_analyze_filtered,2)))=0;
        I_to_analyze = max_quant_data{i}.intensitySum{2};  
        mass_to_analyze_neg = max_quant_data{i}.mass{1};
        mass_to_analyze_neg(max_quant_data{i}.charge{1}>3)=0;
        RT_to_analyze_neg =  max_quant_data{i}.rtApex{1};
        I_to_analyze_neg =  max_quant_data{i}.intensitySum{1};
        RL_to_analyze_neg =  (max_quant_data{i}.rtEnd{1}-max_quant_data{i}.rtStart{1})./2;
        IDX_to_analyze_neg = 1:length(I_to_analyze)';
        output = cell(size(mass_to_analyze));
        if ~GlcPG
            max_quant_split.(unique_raw_file_strings{i}).label_out = output;
            max_quant_split.(unique_raw_file_strings{i}).defect_out = output;
            max_quant_split.(unique_raw_file_strings{i}).C_out = output;
            max_quant_split.(unique_raw_file_strings{i}).RT_out = output;
            max_quant_split.(unique_raw_file_strings{i}).adduct_out = output;
            max_quant_split.(unique_raw_file_strings{i}).final_label_out = output;
            max_quant_split.(unique_raw_file_strings{i}).final_defect_out = zeros(size(mass_to_analyze));
            max_quant_split.(unique_raw_file_strings{i}).final_adduct_out = zeros(size(mass_to_analyze));
        else
            max_quant_split.(unique_raw_file_strings{i}).label_out_Glc = output;
            max_quant_split.(unique_raw_file_strings{i}).defect_out_Glc = output;
            max_quant_split.(unique_raw_file_strings{i}).C_out_Glc = output;
            max_quant_split.(unique_raw_file_strings{i}).adduct_out_Glc = output;
        end
        if ~GlcPG
            num = length(MDs_base);
        else
            num = length(MDs_base_Glc);
        end
        for j = 1:num
            clear match temp_RT temp_RL temp_I temp_idx mass_defect temp_C
            for k = 1:length(MDs_adducts)
               if ~GlcPG
                    match{k} = abs(mass_to_analyze-(MDs_base(j)+MDs_adducts(k))) < ((mass_to_analyze.*ppm_tol)./1000000);
                    mass_defect{k} = mass_to_analyze(match{k})-(MDs_base(j)+MDs_adducts(k));
               else
                    match{k} = abs(mass_to_analyze-(MDs_base_Glc(j)+MDs_adducts(k))) < ((mass_to_analyze.*ppm_tol)./1000000);
                    mass_defect{k} = mass_to_analyze(match{k})-(MDs_base_Glc(j)+MDs_adducts(k));
               end
               temp_RT{k} = RT_to_analyze(match{k});
               temp_RL{k} = RL_to_analyze(match{k});
               temp_I{k} = I_to_analyze(match{k});
               temp_idx{k} = IDX_to_analyze(match{k})';
               temp_C{k} = nonzeros(match{k}.*k);
               if ~GlcPG
                   match{k+length(MDs_adducts)} = abs(mass_to_analyze-(MDs_base(j)+MDs_adducts(k)+decay_mass)) < ((mass_to_analyze.*ppm_tol)./1000000);
                   mass_defect{k+length(MDs_adducts)} = mass_to_analyze(match{k+length(MDs_adducts)})-(MDs_base(j)+MDs_adducts(k)+decay_mass);
                   temp_RT{k+length(MDs_adducts)} =  RT_to_analyze(match{k+length(MDs_adducts)});
                   temp_RL{k+length(MDs_adducts)} = RL_to_analyze(match{k+length(MDs_adducts)});
                   temp_I{k+length(MDs_adducts)} = I_to_analyze(match{k+length(MDs_adducts)});
                   temp_idx{k+length(MDs_adducts)} = IDX_to_analyze(match{k+length(MDs_adducts)})';
                   temp_C{k+length(MDs_adducts)} = nonzeros(match{k+length(MDs_adducts)}.*k);
               end
            end
            if ~GlcPG
                match_neg = abs(mass_to_analyze_neg-(MDs_base(j))) < ((mass_to_analyze_neg.*ppm_tol)./1000000);
            else
                match_neg = abs(mass_to_analyze_neg-(MDs_base_Glc(j))) < ((mass_to_analyze_neg.*ppm_tol)./1000000);
            end
            temp_RT_neg =  RT_to_analyze_neg(match_neg);
            temp_RL_neg = RL_to_analyze_neg(match_neg);   
            temp_I_neg = I_to_analyze_neg(match_neg);   
            cat_RT_no_decay = cat(1,temp_RT{1:length(MDs_adducts)});
            cat_RL_no_decay = cat(1,temp_RL{1:length(MDs_adducts)});
            cat_I_no_decay = cat(1,temp_I{1:length(MDs_adducts)});
            if ~GlcPG
                cat_RT_decay = cat(1,temp_RT{length(MDs_adducts)+1:end});
                cat_RL_decay = cat(1,temp_RL{length(MDs_adducts)+1:end});
                cat_I_decay = cat(1,temp_I{length(MDs_adducts)+1:end});
            end
            
            ids = cat(1,ones(size(cat_RT_no_decay)),ones(size(cat_RT_decay)).*2,ones(size(temp_RT_neg)).*3);
            out = bin_within_range(cat(1,cat_RT_no_decay,cat_RT_decay,temp_RT_neg),cat(1,cat_RL_no_decay,cat_RL_decay,temp_RL_neg));
            
            fitered_species_salts_w_neg = ismember(out(ids==1),out(ids==3));
            
            species_with_neg_no_decay = cat_RT_no_decay(fitered_species_salts_w_neg);
            species_with_neg_no_decay_RL = cat_RL_no_decay(fitered_species_salts_w_neg);
            species_with_neg_no_decay_I = cat_I_no_decay(fitered_species_salts_w_neg);
            
            cat_idx = cat(1,temp_idx{:});
            cat_defect = cat(1,mass_defect{:});
            if ~isempty(species_with_neg_no_decay)
                [sorted_species,sort_idx] = sort(species_with_neg_no_decay);
                bins = bin_within_range(sorted_species,species_with_neg_no_decay_RL(sort_idx));
                edgemembers={};
                for l = 1:max(bins)
                    left_edge = min(sorted_species(bins==l)-species_with_neg_no_decay_RL(bins==l));
                    right_edge = max(sorted_species(bins==l)+species_with_neg_no_decay_RL(bins==l));
                    if abs(left_edge-right_edge)>(RT_tol./20)
                        edgemembers{l} = [left_edge:(RT_tol./20):right_edge];
                    else
                        edgemembers{l} = [left_edge,right_edge];
                    end
                end
                RT_out = {};, RL_out= {};, I_out= {};, idx_out= {};, defect_out = {};, C_out = {};, adduct_out={};
                for l = find(~cellfun(@isempty,edgemembers))
                    RT_out_temp= {};, RL_out_temp= {};, I_out_temp= {};, idx_out_temp= {};, defect_out_temp= {};, in_bounds=0; C_out_temp= {};
                    for k = 1:length(temp_RT)
                        if ~isempty(temp_RT{k})
                            in_bounds = ismembertol(temp_RT{k},edgemembers{l},RT_tol./10,'DataScale',1);
                            RT_out_temp{k} = temp_RT{k}(in_bounds);
                            RL_out_temp{k} = temp_RL{k}(in_bounds);
                            I_out_temp{k} = temp_I{k}(in_bounds);
                            idx_out_temp{k} = temp_idx{k}(in_bounds);
                            defect_out_temp{k} = ones(size(mass_defect{k}(in_bounds))).*mean(mass_defect{k}(in_bounds));
                            C_out_temp{k} = temp_C{k}(in_bounds);
                        end
                    end
                    RT_out{l} = cat(1,RT_out_temp{:});
                    RL_out{l} = cat(1,RL_out_temp{:});
                    I_out{l} = cat(1,I_out_temp{:});
                    idx_out{l} = cat(1,idx_out_temp{:});
                    defect_out{l} = cat(1,defect_out_temp{:});
                    C_out{l} = ones(size(RT_out{l})).*max(cat(1,I_out_temp{:}));
                    adduct_out{l} = cat(1,C_out_temp{:});
                end
                cat_idx_new = cat(1,idx_out{:});
                cat_C_new = cat(1,C_out{:});
                cat_adduct_new = cat(1,adduct_out{:});
                cat_defect_new = cat(1,defect_out{:});
                if ~GlcPG
                    label = PGLabel{j};
                    for l = 1:length(cat_idx_new)
                        temp_label_out = max_quant_split.(unique_raw_file_strings{i}).label_out{cat_idx_new(l)};
                        if ~isempty(temp_label_out)
                            max_quant_split.(unique_raw_file_strings{i}).label_out{cat_idx_new(l)}{length(temp_label_out)+1} = label;
                            max_quant_split.(unique_raw_file_strings{i}).defect_out{cat_idx_new(l)}{length(temp_label_out)+1} = cat_defect_new(l);
                            max_quant_split.(unique_raw_file_strings{i}).C_out{cat_idx_new(l)}{length(temp_label_out)+1} = cat_C_new(l);
                            max_quant_split.(unique_raw_file_strings{i}).adduct_out{cat_idx_new(l)}{length(temp_label_out)+1} = cat_adduct_new(l);
                        else
                            max_quant_split.(unique_raw_file_strings{i}).label_out{cat_idx_new(l)} = {label};
                            max_quant_split.(unique_raw_file_strings{i}).defect_out{cat_idx_new(l)} = {cat_defect_new(l)};
                            max_quant_split.(unique_raw_file_strings{i}).C_out{cat_idx_new(l)} = {cat_C_new(l)};
                            max_quant_split.(unique_raw_file_strings{i}).adduct_out{cat_idx_new(l)} = {cat_adduct_new(l)};
                        end
                    end
                else
                    label = PGLabel_Glc{j};
                    for l = 1:length(cat_idx_new)
                        temp_label_out = max_quant_split.(unique_raw_file_strings{i}).label_out_Glc{cat_idx_new(l)};
                        if ~isempty(temp_label_out)
                            max_quant_split.(unique_raw_file_strings{i}).label_out_Glc{cat_idx_new(l)}{length(temp_label_out)+1} = label;
                            max_quant_split.(unique_raw_file_strings{i}).defect_out_Glc{cat_idx_new(l)}{length(temp_label_out)+1} = cat_defect_new(l);
                            max_quant_split.(unique_raw_file_strings{i}).C_out_Glc{cat_idx_new(l)}{length(temp_label_out)+1} = cat_C_new(l);
                            max_quant_split.(unique_raw_file_strings{i}).adduct_out_Glc{cat_idx_new(l)}{length(temp_label_out)+1} = cat_adduct_new(l);
                        else
                            max_quant_split.(unique_raw_file_strings{i}).label_out_Glc{cat_idx_new(l)} = {label};
                            max_quant_split.(unique_raw_file_strings{i}).defect_out_Glc{cat_idx_new(l)} = {cat_defect_new(l)};
                            max_quant_split.(unique_raw_file_strings{i}).C_out_Glc{cat_idx_new(l)} = {cat_C_new(l)};
                            max_quant_split.(unique_raw_file_strings{i}).adduct_out_Glc{cat_idx_new(l)} = {cat_adduct_new(l)};
                        end
                    end
                end
            end
        end
        if GlcPG
            fprintf('consolidating decay/glucosaminidase activity...')
            for j = find(~cellfun(@isempty, max_quant_split.(unique_raw_file_strings{i}).label_out_Glc))'                   
                label_out = max_quant_split.(unique_raw_file_strings{i}).label_out{j};
                label_out_Glc = max_quant_split.(unique_raw_file_strings{i}).label_out_Glc{j};
                defect_out  = max_quant_split.(unique_raw_file_strings{i}).defect_out{j};
                defect_out_Glc  = max_quant_split.(unique_raw_file_strings{i}).defect_out_Glc{j}; 
                C_out = max_quant_split.(unique_raw_file_strings{i}).C_out{j};
                C_out_Glc  = max_quant_split.(unique_raw_file_strings{i}).C_out_Glc{j}; 
                adduct_out = max_quant_split.(unique_raw_file_strings{i}).adduct_out{j};
                adduct_out_Glc  = max_quant_split.(unique_raw_file_strings{i}).adduct_out_Glc{j}; 
                if ~isempty(label_out) && ~isempty(label_out_Glc) && sum(~ismember(label_out,label_out_Glc))~=0
                   max_quant_split.(unique_raw_file_strings{i}).label_out{j} = cat(2,label_out{:},strcat(label_out_Glc,'-Glc'));
                   max_quant_split.(unique_raw_file_strings{i}).defect_out{j} = {defect_out{:},defect_out_Glc{:}};
                   max_quant_split.(unique_raw_file_strings{i}).C_out{j} = {C_out{:},C_out_Glc{:}};
                   max_quant_split.(unique_raw_file_strings{i}).adduct_out{j} = {adduct_out{:},adduct_out_Glc{:}};
                elseif isempty(label_out) && ~isempty(label_out_Glc)
                    max_quant_split.(unique_raw_file_strings{i}).label_out{j} = strcat(label_out_Glc,'-Glc');
                    max_quant_split.(unique_raw_file_strings{i}).defect_out{j} = defect_out_Glc;
                    max_quant_split.(unique_raw_file_strings{i}).C_out{j} = C_out_Glc;
                    max_quant_split.(unique_raw_file_strings{i}).adduct_out{j} = adduct_out_Glc;
                end
            end
            fprintf('consolidating species..')
            for j = find(~cellfun(@isempty, max_quant_split.(unique_raw_file_strings{i}).label_out))'
                label_out = max_quant_split.(unique_raw_file_strings{i}).label_out{j};
                defect_out  = max_quant_split.(unique_raw_file_strings{i}).defect_out{j};
                defect_temp = [defect_out{:}];
                C_out = max_quant_split.(unique_raw_file_strings{i}).C_out{j};
                adduct_out = max_quant_split.(unique_raw_file_strings{i}).adduct_out{j};
                adduct_temp = [adduct_out{:}];
                num = max([C_out{:}]);
                temptemp = defect_temp([C_out{:}]==num);
                [~,min_num] = min(abs(temptemp));
                idx = find(defect_temp==temptemp(min_num),1);
                max_quant_split.(unique_raw_file_strings{i}).final_label_out{j} = max_quant_split.(unique_raw_file_strings{i}).label_out{j}{idx};
                max_quant_split.(unique_raw_file_strings{i}).final_defect_out(j) = max_quant_split.(unique_raw_file_strings{i}).defect_out{j}{idx};
                max_quant_split.(unique_raw_file_strings{i}).final_adduct_out(j) = max_quant_split.(unique_raw_file_strings{i}).adduct_out{j}{idx};
            end
            no_adduct = max_quant_split.(unique_raw_file_strings{i}).final_adduct_out==1;
            for j = 1:length(no_adduct)
                if no_adduct(j)~=1
                    max_quant_split.(unique_raw_file_strings{i}).final_label_out{j} = '';
                    max_quant_split.(unique_raw_file_strings{i}).final_defect_out(j) = Inf;
                end
            end

            label_temp = {max_quant_split.(unique_raw_file_strings{i}).final_label_out{:}};
            final_unique_labels = unique(label_temp(~cellfun(@isempty,label_temp)));
            max_quant_split.(unique_raw_file_strings{i}).final_unique_labels = final_unique_labels;
            label_temp = cellfun(@(x) strcat(x,''),label_temp,'UniformOutput',0);
            for j = 1:length(final_unique_labels)
                RT_data = max_quant_data{i}.rtApex{2}(ismember(label_temp,final_unique_labels{j}))';
                I_data = max_quant_data{i}.intensitySum{2}(ismember(label_temp,final_unique_labels{j}))';
                RT_bins = bin_within_range(RT_data,RT_tol.*ones(size(RT_data)));
                for l = 1:max(RT_bins)
                    max_quant_split.(unique_raw_file_strings{i}).RT_out_final(j,l) = mean(RT_data(RT_bins==l));
                    max_quant_split.(unique_raw_file_strings{i}).I_out_final(j,l) = sum(I_data(RT_bins==l));
                end
                max_quant_split.(unique_raw_file_strings{i}).I_out_final_sum(j) = sum(I_data);
            end
            fprintf('generating crosslink index...')
            if ~isempty(final_unique_labels)
                unmodified_labels = final_unique_labels(~contains(final_unique_labels,{'Glc','anh','phos'}));
                unmodified_I= max_quant_split.(unique_raw_file_strings{i}).I_out_final_sum(~contains(final_unique_labels,{'Glc','anh','phos'}));
                xlinking = cellfun(@(x) count(x,'X'),unmodified_labels)+1;
                if ~isempty(xlinking)
                    max_quant_split.(unique_raw_file_strings{i}).xlinking_raw = accumarray(xlinking',unmodified_I');
                end
            end
        end
    end
    fprintf('\n')
end

%%
names_mat = {'1SB-1','431SB-1','461-1','523-1','526-1','541-1','542-1','1SC-1','431SC-1','1UC-1','431UC-1','1SB-2','431SB-2','461-2','523-2','526-2','541-2','542-2','1SC-2','431SC-2','1UC-2','431UC-2','1SB-3','431SB-3','461-3','523-3','526-3','541-3','542-3','1SC-3','431SC-3','1UC-3','431UC-2','NAM-H2O','NAM-T'};
temp_names_split = cellfun(@(x) strsplit(x,'-'),names_mat,'UniformOutput',0);
names_mat_unique = unique(cellfun(@(x) x{1} ,temp_names_split,'UniformOutput',0));

name_idxs = find(startsWith(names_mat_unique,{'1SB','431SB','523','461','526','541','542'}));
for i = 1:length(name_idxs)
    idxs = find(startsWith(names_mat,names_mat_unique{name_idxs(i)}));
    for j = 1:3
        final_unique_labels{j} = max_quant_split.(unique_raw_file_strings{idxs(j)}).final_unique_labels;
    end
    unique_labels{i} = unique(cat(2,final_unique_labels{:})); 
end
%%
unique_labels_final = unique(cat(2,unique_labels{:}))'; 

name_idxs = find(startsWith(names_mat_unique,{'1SB','431SB','523','461','526','541','542'}));

k=7;

idxs = find(startsWith(names_mat,names_mat_unique{name_idxs(k)}));
excel_I_RT=[];
for j = 1:3
    I_outs = zeros(1,size(max_quant_split.(unique_raw_file_strings{idxs(j)}).I_out_final,2));
    RT_outs = zeros(1,size(max_quant_split.(unique_raw_file_strings{idxs(j)}).I_out_final,2));
    for i = 1:length(unique_labels_final)
        searchName = unique_labels_final(i);
        labelVal = find(cellfun(@(x) strcmp(x,searchName),max_quant_split.(unique_raw_file_strings{idxs(j)}).final_unique_labels));
        if ~isempty(labelVal)
            I_outs(i,:) = max_quant_split.(unique_raw_file_strings{idxs(j)}).I_out_final(labelVal,:);
            RT_outs(i,:) = max_quant_split.(unique_raw_file_strings{idxs(j)}).RT_out_final(labelVal,:);
        else
            I_outs(i,:) = zeros(1,size(I_outs,2));
            RT_outs(i,:) = zeros(1,size(I_outs,2));
        end
    end
    excel_I_RT = cat(2,excel_I_RT,RT_outs,I_outs);
end
%%





%%
name_idxs = find(startsWith(names_mat_unique,{'1SB','431SB','523','461','526'}));
for i = 1:length(name_idxs)
    idxs = find(startsWith(names_mat,names_mat_unique{name_idxs(i)}));
    for j = 1:3
        final_unique_labels{j} = max_quant_split.(unique_raw_file_strings{idxs(j)}).final_unique_labels;
        I_out_final{j} = max_quant_split.(unique_raw_file_strings{idxs(j)}).I_out_final;
        %I_sum(j) = sum(max_quant_split.(unique_raw_file_strings{idxs(j)}).I_out_final);
    end
    I_label_mean{i}=[];
    I_label_std{i}=[];
    for k = 1:length(unique_labels_final)
        temp_I_2=[];
        for j = 1:3
            contains_label = ismember(final_unique_labels{j},unique_labels_final{k});
            temp_I_2(j) = sum([I_out_final{j}(contains_label) 0]);
        end
        I_label_mean{i}(k) = mean((temp_I_2./I_sum).*100);
        I_label_std{i}(k) = std((temp_I_2./I_sum).*100);
    end
    I_label_mean{i} = I_label_mean{i}';
    I_label_std{i} = I_label_std{i}';
end
%%
name_idxs = find(startsWith(names_mat_unique,{'1SB','431SB','523','461'}));
for i = 1:length(name_idxs)
    idxs = find(startsWith(names_mat,names_mat_unique{name_idxs(i)}));
    for j = 1:3
        final_unique_labels{j} = max_quant_split.(unique_raw_file_strings{idxs(j)}).final_unique_labels;
        I_out_final{j} = max_quant_split.(unique_raw_file_strings{idxs(j)}).I_out_final;
        I_sum(j) = sum(max_quant_split.(unique_raw_file_strings{idxs(j)}).I_out_final);
    end
    I_label_mean_vals{i}=[];
    for k = 1:length(unique_labels_final)
        temp_I_2=[];
        for j = 1:3
            contains_label = ismember(final_unique_labels{j},unique_labels_final{k});
            temp_I_2(j) = sum([I_out_final{j}(contains_label) 0]);
        end
        I_label_mean_vals{i}(k,:) = (temp_I_2./I_sum).*100;
    end
end

%%
countX = cellfun(@(x) count(x,'X'),unique_labels_final)+1
for j = 1:max(countX)
    for i = 1:4
         linkval_mean(i,j) = mean(sum(I_label_mean_vals{i}(countX==j,:)));
         linkval_std(i,j) = std(sum(I_label_mean_vals{i}(countX==j,:)));
    end
end
linkval_mean = linkval_mean'
linkval_std = linkval_std'
%%


