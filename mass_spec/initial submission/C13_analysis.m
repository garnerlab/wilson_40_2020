homeFolder = uigetdir;
fileNames = dir(homeFolder);
sortTemp = natsortfiles({fileNames.name});
temp2 = contains(sortTemp,'SAM');
finalFileNames = sortTemp(temp2);
clearvars -except finalFileNames homeFolder
%%
for i = 1:numel(finalFileNames)
    MSStruct{i}.mzXML = mzxmlread([homeFolder '/' finalFileNames{i}]);
    [MSStruct{i}.PeakList, MSStruct{i}.Times] = mzxml2peaks(MSStruct{i}.mzXML);
end
%
for i = 1:numel(finalFileNames)
    MSStruct{i}.mzXML = [];
    peakListSize = size(MSStruct{i}.PeakList,1);
    MSStruct{i}.negPeakList = MSStruct{i}.PeakList(1:4:peakListSize-rem(peakListSize,4));
    MSStruct{i}.posPeakList = MSStruct{i}.PeakList(3:4:peakListSize-rem(peakListSize,4));
    MSStruct{i}.PeakList = [];
    temp = cellfun(@sum,MSStruct{i}.posPeakList,'UniformOutput',false);
    for k = 1:size(temp,1)
        MSStruct{i}.TIC(k) = temp{k}(2);
    end  
end
%%
[pnPGMDs,pnPGMDLabels] = generate_compounds_C13(0);
%[pnPGMDC13s,pnPGMDC13Labels] = generate_compounds_C13(1);
%%
notToSearch = {'anh','Glc','Phos'};
toSearch = cat(2,PGFindNameAA{[3 4 5 8]});
toSearch = strcat(toSearch(~contains(toSearch, notToSearch)),'_');
for u = 1:2
    pnPGMDLabelsFinal{u} = pnPGMDLabels{u}(contains(pnPGMDLabels{u},toSearch));
    pnPGMDsFinal{u} = pnPGMDs{u}(contains(pnPGMDLabels{u},toSearch));
    pnPGMDC13sFinal{u} = pnPGMDC13s{u}(contains(pnPGMDLabels{u},toSearch));
end
%%
[pnPGMDs,pnPGMDLabels] = generate_compounds_C13(0);
pnPGMDsFinal = pnPGMDs;
pnPGMDLabelsFinal = pnPGMDLabels;
%%
DaTol = 0.05;    
for i = 1:numel(finalFileNames)
    tic
    peaks{1} = MSStruct{i}.posPeakList;
    peaks{2} = MSStruct{i}.negPeakList;
    for u = 1:2
        PGMDs = pnPGMDsFinal{u};
        %PGMDsC13 = pnPGMDC13sFinal{u};
        clear abuErrorTerm daErrorTerm rawAbu C13Abu maxAbu chargeErrorTerm beforeErrorTerm
        for j = 1:size(peaks{u},1)
            if ~rem(j,100), fprintf(' %d...',j); end
            mzs = peaks{u}{j};
            maxAbu(j) = max(mzs(mzs(:,1)>450,2));
            for k = 1:size(PGMDs,1)
                mzExists = ismembertol(mzs(:,1),PGMDs{k}(:,1),DaTol,'DataScale',1);
                if sum(mzExists)>2
                    chargeCheck = mzs(find(mzExists,1):find(mzExists,1,'last'),:);
                    mzCharge = median(diff(chargeCheck(chargeCheck(:,2)>max(chargeCheck(:,2))./40,1)));
                    tempMzs = mzs(mzExists,:);
                    clear tempDiffMzs
                    for l = 1:sum(mzExists)
                        tempDiffMzs(:,l) = PGMDs{k}(:,1)-tempMzs(l,1);
                    end
                    [mins,idxs] = min(abs(tempDiffMzs),[],2);
                    mzList = tempMzs(unique(idxs(mins<DaTol)),:);

                    clear tempDiffMzs
                    for l = 1:size(mzList,1)
                        tempDiffMzs(:,l) = mzList(l,1)-PGMDs{k}(:,1);
                    end
                    [mins,idxs] = min(abs(tempDiffMzs),[],2);
                    [~,IA,IB] = intersect(mins,accumarray(idxs,mins,[],@min,[]));
                    MDsList = PGMDs{k}(sort(IA),:);

                    revMzExists = ismembertol(MDsList(:,1),mzList(:,1),DaTol,'DataScale',1);

                    if revMzExists(1) & (mzList(1,2)>0)
                        [~,idx] = max(MDsList(:,2));
                        MDsList(:,2) = MDsList(:,2)./MDsList(idx,2);
                        rawAbu(k,j) = mzList(1,2);
                        %C13MDList = PGMDsC13{k};
                        %[~,idx2] = max(C13MDList(:,2));
                        %C13MDList(:,2) = C13MDList(:,2)./C13MDList(idx2,2);
                        %[afterPeakMin, afterPeakIdx] = min(abs(mzs(:,1)-C13MDList(idx,1)));
                        %if afterPeakMin<(DaTol)
                        %    C13Abu(k,j) =  mzs(afterPeakIdx,2);
                        %else
                        %    C13Abu(k,j) = 0;
                        %end
                        %rawAbu(k,j) = sum(mzList(:,2));
                        mzList(:,2) = mzList(:,2)./mzList(idx,2);
                        abuErrorTerm(k,j) = sum(abs(mzList(:,2)-MDsList(:,2)));
                        daErrorTerm(k,j) = sum(abs(mzList(:,1)-MDsList(:,1)));
                        MDCharge = mean(diff(PGMDs{k}(:,1)));
                        chargeErrorTerm(k,j) = abs(mzCharge-MDCharge);
                        [beforePeakMin, beforePeakIdx] = min(abs(mzs(:,1)-(PGMDs{k}(1,1)-MDCharge)));
                        if beforePeakMin<DaTol
                            beforeErrorTerm(k,j) = mzs(beforePeakIdx,2);
                        else
                            beforeErrorTerm(k,j) = 0;
                        end
                    else
                        abuErrorTerm(k,j) = Inf;
                        daErrorTerm(k,j) = Inf;
                        beforeErrorTerm(k,j) = Inf;
                        chargeErrorTerm(k,j) = Inf;
                        %C13Abu(k,j) = 0;
                    end
                else
                    abuErrorTerm(k,j) = Inf;
                    daErrorTerm(k,j) = Inf;
                    beforeErrorTerm(k,j) = Inf;
                    chargeErrorTerm(k,j) = Inf;
                    %C13Abu(k,j) = 0;
                end
            end
        end
        results{u,i}.abuErrorTerm = abuErrorTerm;
        results{u,i}.daErrorTerm = daErrorTerm;
        results{u,i}.rawAbu = rawAbu;
        %results{u,i}.C13Abu = C13Abu;
        results{u,i}.maxAbu = maxAbu;
        results{u,i}.beforeErrorTerm = beforeErrorTerm;
        results{u,i}.chargeErrorTerm = chargeErrorTerm;
    end
    toc
end
%%
for i = 1:PGsize
    counter = 1;
    PGFindName{i}{counter} = PG{i}.Name;
    if isfield(PG{i},'Glc')
        for k = 1:size(PG{i}.Glc,2)
            counter = counter+1;
            PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'Glc');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'Glc',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'anh')
        for k = 1:size(PG{i}.anh,2)
            counter = counter+1;
            PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'anh');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'anh',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'Phos')
        for k = 1:size(PG{i}.Phos,2)
            counter = counter+1;
            PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'Phos');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'Phos',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'Acyl')
        for k = 1:size(PG{i}.Acyl,2)
            counter = counter+1;
            PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'Acyl');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'Acyl',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'NH2')
        for k = 1:size(PG{i}.NH2,2)
            counter = counter+1;
            PGFindName{i}{counter} = strcat(PG{i}.Name,num2str(k),'NH2');
        end
    end
end

for i = lol:PGsize
    counter = 1;
    PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A');
    if isfield(PG{i},'Glc')
        for k = 1:size(PG{i}.Glc,2)
            counter = counter+1;
            PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'Glc');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'Glc',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'anh')
        for k = 1:size(PG{i}.anh,2)
            counter = counter+1;
            PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'anh');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'anh',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'Phos')
        for k = 1:size(PG{i}.Phos,2)
            counter = counter+1;
            PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'Phos');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'Phos',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'Acyl')
        for k = 1:size(PG{i}.Acyl,2)
            counter = counter+1;
            PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'Acyl');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'Acyl',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'NH2')
        for k = 1:size(PG{i}.NH2,2)
            counter = counter+1;
            PGFindNameA{i}{counter} = strcat(PG{i}.Name,'A',num2str(k),'NH2');
        end
    end
end

for i = lol:PGsize
    counter = 1;
    PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA');
    if isfield(PG{i},'Glc')
        for k = 1:size(PG{i}.Glc,2)
            counter = counter+1;
            PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'Glc');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'Glc',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'anh')
        for k = 1:size(PG{i}.anh,2)
            counter = counter+1;
            PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'anh');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'anh',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'Phos')
        for k = 1:size(PG{i}.Phos,2)
            counter = counter+1;
            PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'Phos');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'Phos',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'Acyl')
        for k = 1:size(PG{i}.Acyl,2)
            counter = counter+1;
            PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'Acyl');
            if isfield(PG{i},'NH2')
                for j = 1:size(PG{i}.NH2,2)
                    counter = counter+1;
                    PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'Acyl',num2str(j),'NH2');
                end
            end
        end
    end
    if isfield(PG{i},'NH2')
        for k = 1:size(PG{i}.NH2,2)
            counter = counter+1;
            PGFindNameAA{i}{counter} = strcat(PG{i}.Name,'AA',num2str(k),'NH2');
        end
    end
end
%
for i = 1:(lol-1)
    PGFindNameAA{i} = PGFindName{i};
    PGFindNameA{i} = PGFindName{i};
end
%%
for k = [3 4 5 8]
    notToSearch = {'anh','Glc','Phos'};
    namesToSearch = PGFindNameAA{k}(~contains(PGFindNameAA{k},notToSearch));
    upSearchNameAA = strcat(namesToSearch,'_');
    upSearchNameC13 = strcat(namesToSearch,'&C13_');
    upSearchNameTemp = {upSearchNameAA,upSearchNameC13};
    clear totalAbundOutSave
    for kk = 1:2
        upSearchName = upSearchNameTemp{kk};
        for l = 1:numel(upSearchName)
            clear totalAbundOut
            searchName = upSearchName{l};
                for i = 1:10
                    clear sumStore relSumStore
                    %upSearchName = upSearchNameTemp{kk};
                    %searchName = upSearchName{l};
                    for u = 1:2
                        pPGnum = find(contains(pnPGMDLabelsFinal{u},strcat(searchName)));
                        clear  abuRelError rawAbu relAbu maxAbu chargeErrorTerm beforeErrorTerm relAbuThresh rawAbuThresh threshMat abuErrorTerm daErrorTerm 
                        threshMat = zeros(size(results{u,i}.abuErrorTerm,2), numel(pPGnum));
                        rawAbu = threshMat;
                        relAbu = threshMat;
                        for ii = 1:numel(pPGnum)
                            MD = pnPGMDs{u}{pPGnum(ii)}(1);
                            abuErrorTerm(:,ii) = results{u,i}.abuErrorTerm(pPGnum(ii),:);
                            daErrorTerm(:,ii) = results{u,i}.daErrorTerm(pPGnum(ii),:);
                            rawAbu(:,ii) = results{u,i}.rawAbu(pPGnum(ii),:);
                            maxAbu = results{u,i}.maxAbu;
                            beforeErrorTerm(:,ii) = results{u,i}.beforeErrorTerm(pPGnum(ii),:)';
                            chargeErrorTerm(:,ii) = results{u,i}.chargeErrorTerm(pPGnum(ii),:)';
                            relAbu(:,ii) = (rawAbu(:,ii)./(maxAbu'))';
                            abuRelError(:,ii) = rawAbu(:,ii)./beforeErrorTerm(:,ii);
                            threshMat(:,ii) = (abuRelError(:,ii)>2)&(chargeErrorTerm(:,ii)<0.1)&(daErrorTerm(:,ii)<(MD./12000))&(abuErrorTerm(:,ii)<10);
                        end
                        if u == 1
                            sumThresh = sum(threshMat,2)>1;
                        else 
                            sumThresh = sum(threshMat,2)>0;
                        end
                        %sumThresh(1:500)=0;
                        %sumThresh(3200:end)=0;
                        sumStore{u} = sum(rawAbu.*sumThresh,2);
                        relSumStore{u} = sum(relAbu.*sumThresh,2);
                    end
                    relSum = smooth(sum([relSumStore{:}]>0,2),10)>0.01;
                    totalAbundOut(i,:) = sum([sumStore{:}].*relSum);
                    %plot(smooth(sum([sumStore{:}].*relSum,2),10),'Color',color(shift(kk,l),:))
                    %hold on
                end
            totalAbundOutSave{kk,l} = totalAbundOut';
        end
    end
    %outAns(k,:) = sum(cat(1,totalAbundOutSave{:}));
    %
    catAbund1 = cat(1,totalAbundOutSave{1,:});
    catAbund2 = cat(1,totalAbundOutSave{2,:});
    %
    %temp1 = sum(catAbund2(1:2:end,:))./(sum(catAbund1(1:2:end,:))+sum(catAbund2(1:2:end,:)));
    %temp2 = sum(catAbund2(2:2:end,:))./(sum(catAbund1(2:2:end,:))+sum(catAbund2(2:2:end,:)));
    temp1 = sum(catAbund2(1:2:end,:))./(sum(catAbund1(1:2:end,:)));
    temp2 = sum(catAbund2(2:2:end,:))./(sum(catAbund1(2:2:end,:)));
    
    %temp3(k,:) = mean(cat(1,smooth(temp1)',smooth(temp2)'));
    temp3(k,:) = mean(cat(1,temp1,temp2));
    
    plot(log2(meanOD(1:end)./meanOD(1)),temp3(k,:),'LineWidth',3)
    %plot(log2(meanOD(1:end)./meanOD(1)),catAbund2(1,:)./catAbund2(1,end))
    hold on
end
%%
temp3(1,:) = mean([temp3(3,:);temp3(5,:)]);
for k = [1 4 8]
    loglog(log2(meanOD(1:end)./meanOD(1)),temp3(k,:),'LineWidth',3)
    hold on
end
%title('rate of heavy isotope labeled GlcNAc accumulation')
xlabel('generations')
ylabel('proportion of heavy GlcNAc ')
legend({'DL-EP product','uncrosslinked PG','crosslinked PG'})
set(gca,'FontSize',20)
%%
for k = [3 4 5 8]
    num = [0:0.25:1.3];
    Xdata = cat(1,log2(dataOut{1}.meanOD./dataOut{1}.meanOD(1)),log2(dataOut{2}.meanOD./dataOut{2}.meanOD(1)));
    Ydata = cat(2,dataOut{1}.temp3(k,:),dataOut{2}.temp3(k,:))';
    Xbins = discretize(Xdata, num);
    clear binnedData
    binnedData = zeros(length(num), 4);
    for i = 1:length(num)
        binnedData(i,1) = nanmean(Xdata(Xbins==i));
        binnedData(i,2) = nanstd(Xdata(Xbins==i))./(sqrt(length(Xdata(Xbins==i))));
        binnedData(i,3) = nanmean(Ydata(Xbins==i));
        binnedData(i,4) = nanstd(Ydata(Xbins==i))./(sqrt(length(Xdata(Xbins==i))));
        binnedData(i,5) = numel(Ydata(Xbins==i));
    end
    errorbar(binnedData(:,1), binnedData(:,3), binnedData(:,4),'LineWidth',3)%, binnedData(:,4), binnedData(:,2), binnedData(:,2)
    hold on
end
%set(gca, 'YScale', 'log','XScale', 'log')
xlabel('generations')
ylabel('proportion of heavy GlcNAc ')
legend({'DL-EP product','uncrosslinked PG','crosslinked PG'})
set(gca,'FontSize',20)

%%
for k = [1 4 8]
    num = [0:0.15:1.3];
    Xdata = cat(1,log2(dataOut{1}.meanOD./dataOut{1}.meanOD(1)),log2(dataOut{2}.meanOD./dataOut{2}.meanOD(1)));
    Ydata = cat(2,dataOut{1}.temp3(k,:),dataOut{2}.temp3(k,:))';
    Xbins = discretize(Xdata, num);
    clear binnedData
    binnedData = zeros(length(num), 4);
    for i = 1:length(num)
        binnedData(i,1) = nanmean(Xdata(Xbins==i));
        binnedData(i,2) = nanstd(Xdata(Xbins==i))./(sqrt(length(Xdata(Xbins==i))));
        binnedData(i,3) = nanmean(Ydata(Xbins==i));
        binnedData(i,4) = nanstd(Ydata(Xbins==i))./(sqrt(length(Xdata(Xbins==i))));
    end
    plot((binnedData(2:end,1)+(diff(binnedData(:,1))./2)), diff(smooth(binnedData(:,3))),'LineWidth',3)
    %errorbar(binnedData(:,1), binnedData(:,3), binnedData(:,4),'LineWidth',3)%, binnedData(:,4), binnedData(:,2), binnedData(:,2)
    hold on
end
xlabel('generations')
ylabel('rate of heavy GlcNAc accumulation')
legend({'DL-EP product','uncrosslinked PG','crosslinked PG'})
set(gca,'FontSize',20)