wallStrings = {
                'M'
                'MA'
                'MAG'
                'MAGD'
                'MAGDADTX'
                'MAGDADGTX'
                'MAGDADGATX'
                'MAGDADGAMX'
                'MAGDADGAMADGAMXX'
                'MAGDADGAMADGAMADGAMXXX'};
%
Elements = [19	34	2	13	0	0%M
            -8	-13	-1	-5	0	0%-Glc
            -2	-2	0	-1	0	0%Ac
            0	0	0	3	1	0%P
            0	-4	0	-1	0	0%anh
            3	5	1	1	0	0%Ala
            5	7	1	3	0	0%Glu
            7	12	2	3	0	0%Dap
            0	-2	0	-1	0	0%Xlink
            0	2	0	1	0	0%T
            0	1	1	-1	0	0%NH2
            0	1	0	0	0	0];%H
Symbols = {'C' 'H'	'N'	'O'	'P'	'Na'};
Names = {'M' '-Glc' 'Ac' 'P' 'anh' 'A' 'G' 'D' 'X' 'T' 'NH2' 'H'};

tetramer = [0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
            0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
            0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
            0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0
            0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0
            0	0	0	0	0	0	1	0	0	1	0	0	0	0	0	0	0	0	0
            0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0
            0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0
            0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
            0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0
            0	0	0	0	0	0	0	0	0	0	0	1	0	0	1	0	0	0	0
            0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0
            0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0
            0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
            0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0
            0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0
            0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0
            0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1
            0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0];
ElemStr = {'M1'	'A1'	'G1'	'D1'	'A21'	'D2'	'G2'	'A22'	'M2'	'A31'	'D3'	'G3'	'A32'	'M3'	'A41'	'D4'	'G4'	'A4'	'M4'};
G = graph(tetramer,ElemStr,'upper');
%
clear PG
%{
for i = 1:(size(G.Edges,1)+1)
    if i<=size(G.Edges,1)
        H = rmedge(G,i);
    else
        H=G;
    end
    cc = conncomp(H,'OutputForm','cell');
    elemNames = cc{1};
    clear letter number
    for j = 1:numel(elemNames)
        letter{j} = elemNames{j}(1);
        number(j) = str2num(elemNames{j}(2));
    end
    for k = 1:max(number-1)
        letter{j+k} = 'X';
    end
    PG{i}.Elements = zeros(1,size(Elements,2));
    for j = 1:numel(letter)
        switch letter{j}
            case 'M'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'M'),:);
            case 'A'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'A'),:);
            case 'G'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'G'),:);
            case 'D'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'D'),:);
            case 'X'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'X'),:);
        end
    end
    PG{i}.Name = strcat(letter{:});
end
%}
for i = 1:size(wallStrings,1)
    letter = wallStrings{i};
    PG{i}.Elements = zeros(1,size(Elements,2));
    for j = 1:numel(letter)
        switch letter(j)
            case 'M'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'M'),:);
            case 'A'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'A'),:);
            case 'G'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'G'),:);
            case 'D'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'D'),:);
            case 'X'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'X'),:);
            case 'T'
                PG{i}.Elements = PG{i}.Elements + Elements(strcmp(Names,'T'),:);
        end
    end
    PG{i}.Name = letter;
end
PGsize = size(PG,2);
%
for i = 3:PGsize
    PG{i+PGsize-2}.Elements = PG{i}.Elements + Elements(strcmp(Names,'A'),:);
    PG{i+PGsize-2}.Name = strcat(PG{i}.Name , 'A');
    PG{i+(2.*PGsize)-4}.Elements = PG{i}.Elements + 2.*Elements(strcmp(Names,'A'),:);
    PG{i+(2.*PGsize)-4}.Name = strcat(PG{i}.Name , 'AA');
end
%
for i = 1:size(PG,2)
        PG{i}.Glc{1} = PG{i}.Elements + Elements(strcmp(Names,'-Glc'),:);
        PG{i}.Phos{1} = PG{i}.Elements + Elements(strcmp(Names,'P'),:);
        PG{i}.anh{1} = PG{i}.Elements + Elements(strcmp(Names,'anh'),:);
    for j = 1:sum(PG{i}.Name=='M')
        PG{i}.Acyl{j} = PG{i}.Elements + j.*Elements(strcmp(Names,'Ac'),:);
    end
end
%
for i = 1:size(PG,2)
    for j = 1:sum(PG{i}.Name=='D')
        PG{i}.NH2{j} = PG{i}.Elements + j.*Elements(strcmp(Names,'NH2'),:);
        if isfield(PG{i},'Acyl')
            for k = 1:size(PG{i}.Acyl,2)
                PG{i}.NH2Acyl{j,k} = PG{i}.Acyl{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
        if isfield(PG{i},'Glc')
            for k = 1:size(PG{i}.Glc,2)
                PG{i}.NH2Glc{j,k} = PG{i}.Glc{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
        if isfield(PG{i},'Phos')
            for k = 1:size(PG{i}.Phos,2)
                PG{i}.NH2Phos{j,k} = PG{i}.Phos{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
        if isfield(PG{i},'anh')
            for k = 1:size(PG{i}.anh,2)
                PG{i}.NH2anh{j,k} = PG{i}.anh{k} + j.*Elements(strcmp(Names,'NH2'),:);
            end
        end
    end
end
%
clear counter Compound PGLabel
counter = 1;
for i = 1:size(PG,2)
    PGLabel{counter} = PG{i}.Name;
    Compound(counter).C = PG{i}.Elements(1);
    Compound(counter).H = PG{i}.Elements(2);
    Compound(counter).N = PG{i}.Elements(3);
    Compound(counter).O = PG{i}.Elements(4);
    Compound(counter).P = PG{i}.Elements(5);
    Compound(counter).Na = PG{i}.Elements(6);
    counter = counter+1;
    if isfield(PG{i},'NH2')
        for PGnh2 = 1:size(PG{i}.NH2,2)
            PGLabel{counter} = strcat(PG{i}.Name,num2str(PGnh2),'NH2');
            Compound(counter).C = PG{i}.NH2{PGnh2}(1);
            Compound(counter).H = PG{i}.NH2{PGnh2}(2);
            Compound(counter).N = PG{i}.NH2{PGnh2}(3);
            Compound(counter).O = PG{i}.NH2{PGnh2}(4);
            Compound(counter).P = PG{i}.NH2{PGnh2}(5);
            Compound(counter).Na = PG{i}.NH2{PGnh2}(6);
            counter = counter+1;
        end
    end
    if isfield(PG{i},'Acyl')
        for Acyl = 1:size(PG{i}.Acyl,2)
            PGLabel{counter} = strcat(PG{i}.Name,num2str(Acyl),'Acyl');
            Compound(counter).C = PG{i}.Acyl{Acyl}(1);
            Compound(counter).H = PG{i}.Acyl{Acyl}(2);
            Compound(counter).N = PG{i}.Acyl{Acyl}(3);
            Compound(counter).O = PG{i}.Acyl{Acyl}(4);
            Compound(counter).P = PG{i}.Acyl{Acyl}(5);
            Compound(counter).Na = PG{i}.Acyl{Acyl}(6);
            counter = counter+1;
        end
    end
    if isfield(PG{i},'Glc')
        for Glc = 1:size(PG{i}.Glc,2)
            PGLabel{counter} = strcat(PG{i}.Name,num2str(Glc),'Glc');
            Compound(counter).C = PG{i}.Glc{Glc}(1);
            Compound(counter).H = PG{i}.Glc{Glc}(2);
            Compound(counter).N = PG{i}.Glc{Glc}(3);
            Compound(counter).O = PG{i}.Glc{Glc}(4);
            Compound(counter).P = PG{i}.Glc{Glc}(5);
            Compound(counter).Na = PG{i}.Glc{Glc}(6);
            counter = counter+1;
        end
    end
    if isfield(PG{i},'Phos')
        for Phos = 1:size(PG{i}.Phos,2)
            PGLabel{counter} = strcat(PG{i}.Name,num2str(Phos),'Phos');
            Compound(counter).C = PG{i}.Phos{Phos}(1);
            Compound(counter).H = PG{i}.Phos{Phos}(2);
            Compound(counter).N = PG{i}.Phos{Phos}(3);
            Compound(counter).O = PG{i}.Phos{Phos}(4);
            Compound(counter).P = PG{i}.Phos{Phos}(5);
            Compound(counter).Na = PG{i}.Phos{Phos}(6);
            counter = counter+1;
        end
    end
    if isfield(PG{i},'anh')
        for anh = 1:size(PG{i}.anh,2)
            PGLabel{counter} = strcat(PG{i}.Name,num2str(anh),'anh');
            Compound(counter).C = PG{i}.anh{anh}(1);
            Compound(counter).H = PG{i}.anh{anh}(2);
            Compound(counter).N = PG{i}.anh{anh}(3);
            Compound(counter).O = PG{i}.anh{anh}(4);
            Compound(counter).P = PG{i}.anh{anh}(5);
            Compound(counter).Na = PG{i}.anh{anh}(6);
            counter = counter+1;
        end
    end
    if isfield(PG{i},'NH2anh')
        for NH2anh = 1:size(PG{i}.NH2anh,2)
            for anh = 1:size(PG{i}.NH2anh,1)
                PGLabel{counter} = strcat(PG{i}.Name,num2str(NH2anh),'anh',num2str(anh),'NH2');
                Compound(counter).C = PG{i}.NH2anh{anh,NH2anh}(1);
                Compound(counter).H = PG{i}.NH2anh{anh,NH2anh}(2);
                Compound(counter).N = PG{i}.NH2anh{anh,NH2anh}(3);
                Compound(counter).O = PG{i}.NH2anh{anh,NH2anh}(4);
                Compound(counter).P = PG{i}.NH2anh{anh,NH2anh}(5);
                Compound(counter).Na = PG{i}.NH2anh{anh,NH2anh}(6);
                counter = counter+1;
            end
        end
    end
    if isfield(PG{i},'NH2Phos')
        for NH2anh = 1:size(PG{i}.NH2Phos,2)
            for anh = 1:size(PG{i}.NH2Phos,1)
                PGLabel{counter} = strcat(PG{i}.Name,num2str(NH2anh),'Phos',num2str(anh),'NH2');
                Compound(counter).C = PG{i}.NH2Phos{anh,NH2anh}(1);
                Compound(counter).H = PG{i}.NH2Phos{anh,NH2anh}(2);
                Compound(counter).N = PG{i}.NH2Phos{anh,NH2anh}(3);
                Compound(counter).O = PG{i}.NH2Phos{anh,NH2anh}(4);
                Compound(counter).P = PG{i}.NH2Phos{anh,NH2anh}(5);
                Compound(counter).Na = PG{i}.NH2Phos{anh,NH2anh}(6);
                counter = counter+1;
            end
        end
    end
    if isfield(PG{i},'NH2Glc')
        for NH2anh = 1:size(PG{i}.NH2Glc,2)
            for anh = 1:size(PG{i}.NH2Glc,1)
                PGLabel{counter} = strcat(PG{i}.Name,num2str(NH2anh),'Glc',num2str(anh),'NH2');
                Compound(counter).C = PG{i}.NH2Glc{anh,NH2anh}(1);
                Compound(counter).H = PG{i}.NH2Glc{anh,NH2anh}(2);
                Compound(counter).N = PG{i}.NH2Glc{anh,NH2anh}(3);
                Compound(counter).O = PG{i}.NH2Glc{anh,NH2anh}(4);
                Compound(counter).P = PG{i}.NH2Glc{anh,NH2anh}(5);
                Compound(counter).Na = PG{i}.NH2Glc{anh,NH2anh}(6);
                counter = counter+1;
            end
        end
    end
    if isfield(PG{i},'NH2Acyl')
        for NH2anh = 1:size(PG{i}.NH2Acyl,2)
            for anh = 1:size(PG{i}.NH2Acyl,1)
                PGLabel{counter} = strcat(PG{i}.Name,num2str(NH2anh),'Acyl',num2str(anh),'NH2');
                Compound(counter).C = PG{i}.NH2Acyl{anh,NH2anh}(1);
                Compound(counter).H = PG{i}.NH2Acyl{anh,NH2anh}(2);
                Compound(counter).N = PG{i}.NH2Acyl{anh,NH2anh}(3);
                Compound(counter).O = PG{i}.NH2Acyl{anh,NH2anh}(4);
                Compound(counter).P = PG{i}.NH2Acyl{anh,NH2anh}(5);
                Compound(counter).Na = PG{i}.NH2Acyl{anh,NH2anh}(6);
                counter = counter+1;
            end
        end
    end
end
%
clear CompoundpH Compoundp2H Compoundp3H
clear CompoundnH Compoundn2H Compoundn3H
clear CompoundpNa Compoundp2Na Compoundp3Na
clear PGLabelpH PGLabelp2H PGLabelp3H
clear PGLabelnH PGLabeln2H PGLabeln3H
clear PGLabelpNa PGLabelp2Na PGLabelp3Na
for i = 1:size(Compound,2)
    CompoundpH(i) = Compound(i);
    CompoundpH(i).H = Compound(i).H+1;
    PGLabelpH{i} = strcat(PGLabel{i},'_H+');
    
    Compoundp2H(i) = Compound(i);
    Compoundp2H(i).H = Compound(i).H+2;
    PGLabelp2H{i} = strcat(PGLabel{i},'_2H+');
    
    Compoundp3H(i) = Compound(i);
    Compoundp3H(i).H = Compound(i).H+3;
    PGLabelp3H{i} = strcat(PGLabel{i},'_3H+');
    
    CompoundpNa(i) = Compound(i);
    CompoundpNa(i).Na = Compound(i).Na+1;
    PGLabelpNa{i} = strcat(PGLabel{i},'_Na+');
    
    Compoundp2Na(i) = Compound(i);
    Compoundp2Na(i).Na = Compound(i).Na+2;
    PGLabelp2Na{i} = strcat(PGLabel{i},'_2Na+');
    
    Compoundp3Na(i) = Compound(i);
    Compoundp3Na(i).Na = Compound(i).Na+3;
    PGLabelp3Na{i} = strcat(PGLabel{i},'_3Na+');
    
    CompoundnH(i) = Compound(i);
    CompoundnH(i).H = Compound(i).H-1;
    PGLabelnH{i} = strcat(PGLabel{i},'_H-');
    
    Compoundn2H(i) = Compound(i);
    Compoundn2H(i).H = Compound(i).H-2;
    PGLabeln2H{i} = strcat(PGLabel{i},'_2H-');
        
    Compoundn3H(i) = Compound(i);
    Compoundn3H(i).H = Compound(i).H-3;
    PGLabeln3H{i} = strcat(PGLabel{i},'_3H-');
    
end
%
'isotopicdists'
pPGMDs = isotopicdist_better(cat(2,CompoundpH,CompoundpNa));
'p1'
%
p2PGMDs = isotopicdist_better(cat(2,Compoundp2H,Compoundp2Na));
'p2'
p3PGMDs = isotopicdist_better(cat(2,Compoundp3H,Compoundp3Na));
'p3'
%
for i = 1:size(p2PGMDs,1)
    p2PGMDs{i}(:,1) = p2PGMDs{i}(:,1)./2;
end
for i = 1:size(p3PGMDs,1)
    p3PGMDs{i}(:,1) = p3PGMDs{i}(:,1)./3;
end
nPGMDs = isotopicdist_better(cat(2,CompoundnH));
'n1'
n2PGMDs = isotopicdist_better(cat(2,Compoundn2H));
'n2'
n3PGMDs = isotopicdist_better(cat(2,Compoundn3H));
'n3'

for i = 1:size(n2PGMDs,1)
    n2PGMDs{i}(:,1) = n2PGMDs{i}(:,1)./2;
end
for i = 1:size(n3PGMDs,1)
    n3PGMDs{i}(:,1) = n3PGMDs{i}(:,1)./3;
end

%
pPGMDsLabel = cat(2,PGLabelpH,PGLabelpNa);
p2PGMDsLabel = cat(2,PGLabelp2H,PGLabelp2Na);
p3PGMDsLabel = cat(2,PGLabelp3H,PGLabelp3Na);

nPGMDsLabel = cat(2,PGLabelnH);
n2PGMDsLabel = PGLabeln2H;
n3PGMDsLabel = PGLabeln3H;
%
pPGMDs = cat(1, pPGMDs, p2PGMDs, p3PGMDs);
pPGMDsLabel = cat(2, pPGMDsLabel, p2PGMDsLabel, p3PGMDsLabel);
%
nPGMDs = cat(1, nPGMDs, n2PGMDs, n3PGMDs);
nPGMDsLabel = cat(2, nPGMDsLabel, n2PGMDsLabel,n3PGMDsLabel);
clearvars -except nPGMDs pPGMDs nPGMDsLabel pPGMDsLabel MSStruct