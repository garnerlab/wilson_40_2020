homeFolder = uigetdir;
fileNames = dir(homeFolder);
sortTemp = natsortfiles({fileNames.name});
temp2 = contains(sortTemp,'mzXML');
finalFileNames = sortTemp(temp2);
clearvars -except finalFileNames homeFolder
%%
for i = 1:numel(finalFileNames)
    MSStruct{i}.mzXML = mzxmlread([homeFolder '/' finalFileNames{i}]);
    [MSStruct{i}.PeakList, MSStruct{i}.Times] = mzxml2peaks(MSStruct{i}.mzXML);
end
%%
for i = 1:numel(finalFileNames)
    temp = cellfun(@sum,MSStruct{i}.PeakList,'UniformOutput',false);
    for k = 1:size(temp,1)
        MSStruct{i}.TIC(k) = temp{k}(2);
    end  
end
%%
i = 1;
finalFileNames{i}
DaTol = 0.4;
%searchWeight = 493.21;
searchWeight = 294.12;
%searchWeight = 451.20;

for j = 1:size(MSStruct{i}.PeakList)
    EIC(j) = sum(MSStruct{i}.PeakList{j}(ismembertol(MSStruct{i}.PeakList{j}(:,1),searchWeight,DaTol,'DataScale',1),2));
end
plot(smooth(EIC))
%%
i = 2;
finalFileNames{i}
for j = 1:size(MSStruct{i}.PeakList)
    EIC(j) = sum(MSStruct{i}.PeakList{j}(ismembertol(MSStruct{i}.PeakList{j}(:,1),searchWeight,DaTol,'DataScale',1),2));
end
hold on
plot(smooth(EIC))
%%
i=2
loc = 130;
hold on
plot(MSStruct{i}.PeakList{loc}(:,1),MSStruct{i}.PeakList{loc}(:,2))